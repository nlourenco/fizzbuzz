function init() {
    var container = document.getElementById("main");
    container.innerHTML = FizzBuzz();
}

function FizzBuzz(container) {
    var result = "";
    for (i = 1; i <= 20; i++) {
        var test = "";
        if(i % 3 === 0) test += "Fizz";
        if(i % 5 === 0) test += "Buzz";
        
        if(i % 3 !== 0 && i % 5 !== 0) test += i;
        
        result += test + "<br>";
    }
    return result;
};